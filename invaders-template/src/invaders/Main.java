package invaders;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.util.ArrayList;
import java.util.Random;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.Timer;

import invaders.sounds.SoundLoader;

public class Main extends JPanel {
	public static void main(String[] args) {
		JFrame frame = new JFrame();
		frame.setContentPane(new Main());
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.pack();
		frame.setVisible(true);
	}
	
	Main() {
		setPreferredSize(new Dimension(600, 900));
		
		MouseListener clickListener = new MouseAdapter() {
			public void mousePressed(MouseEvent event) {
			}
		};
		addMouseListener(clickListener);
		
		MouseMotionListener motionListener = new MouseMotionListener() {
			public void mouseDragged(MouseEvent arg0) {
			}

			public void mouseMoved(MouseEvent event) {
			}
		};
		addMouseMotionListener(motionListener);		
		
		ActionListener actionListener = new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				repaint();
			}
		};
		
		Timer timer = new Timer(20, actionListener);
		timer.start();
	}
	
	public void paintComponent(Graphics g) {
		g.setColor(Color.black);
		g.fillRect(0, 0, 600, 900);
	}
}
